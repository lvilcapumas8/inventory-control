export const registerProductFormSettings = {
  form: '.js-es-register-product-form',
  fieldErrorClass: 'is-danger',
  fields: [
    {
      selector: '.es-input-name',
      rule: 'required',
      message: 'Ingrese un nombre',
    },
    {
      selector: '.es-input-price',
      rule: 'required',
      message: 'Ingrese un precio',
    },
    {
      selector: '.es-input-file',
      rule: 'required',
      message: 'Seleccione una imagen',
    }
  ],
};