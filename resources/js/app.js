require('./bootstrap');
require('./script.js');
window.Vue = require('vue');

import buefy from 'buefy';
import { Table } from 'buefy/dist/components/table';
import { TableColumn } from 'buefy/dist/components/table';
import { Field } from 'buefy/dist/components/field';
import { Icon } from 'buefy/dist/components/icon';
import { Switch } from 'buefy/dist/components/switch';
import { ToastProgrammatic as Toast } from 'buefy/dist/components/toast';

Vue.component('b-table', Table);
Vue.component('b-table-column', TableColumn);
Vue.component('b-field', Field);
Vue.component('b-icon', Icon);
Vue.component('b-switch', Switch);
Vue.component('b-toast', Toast);
Vue.component('example-component', require('./components/vue/ExampleComponent.vue').default);
Vue.component('table-dropdown', require('./components/vue/TableDropdownComponent.vue').default);

Vue.use(buefy);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Field);
Vue.use(Icon);
Vue.use(Switch);
Vue.use(Toast);
Vue.use(require('vue-resource'));

const app = new Vue({
    el: '#app',
    mounted: function() {
        console.log(this.$http);
      }
});
