import { prefix } from '../constants/application';

const nav = () => {
  const bodyEl = document.querySelector(`.js-${prefix}-body`);
  const navEl = document.querySelector(`.js-${prefix}-nav`);
  const hamburgerEl = document.querySelector(`.js-${prefix}-hamburger`);

  if (!navEl || !hamburgerEl) return;

  hamburgerEl.addEventListener('click', (e) => {
    e.preventDefault();
    bodyEl.classList.toggle('mobile-nav-expanded');
  });
};

export default nav;
