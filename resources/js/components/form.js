import { registerProductFormSettings } from '../constants/formSettings';
import validator from '../vendors/validator';

const handleSubmit = (formEl, formSelector, cb) => {
  formEl.addEventListener('submit', (evt) => {
    if (!validator) return;
    if (!validator.isValid(formSelector)) {
      evt.preventDefault();
      return;
    }

    if (cb) {
      cb();
    } else {
      formEl.submit();
    }
  });
};

const registerProductFormValidation = () => {
  const productForm = document.querySelector(registerProductFormSettings.form);
  if (!productForm) return;

  validator.init(registerProductFormSettings);
  handleSubmit(productForm, registerProductFormSettings.form);
};

const form = () => {
  registerProductFormValidation()
};

export default form;
