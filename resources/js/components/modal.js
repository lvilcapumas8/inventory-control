import { prefix } from '../constants/application';

const modal = () => {
  const modalWrapper = document.querySelector(`.js-${prefix}-modal`);

  if (!modalWrapper) return;

  const HTML = document.querySelector('html');
  const modalToggleButtons = document.querySelectorAll(`.js-${prefix}-modal-toggle-button`);

  Array.prototype.forEach.call(modalToggleButtons, (button) => {
    button.addEventListener('click', (e) => {
      e.preventDefault();
      HTML.classList.toggle('modal-expanded');
      const modalEl = document.querySelector(e.currentTarget.dataset.modal);
      modalEl.classList.toggle('is-visible');
    });
  });
};

export default modal;