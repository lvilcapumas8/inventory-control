/*eslint-disable*/

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory();
  } else {
    root.cmoFormValidate = factory();
  }
}(this, () => {
  let options;
  let cb;


  const validations = {
    isNotEmpty(val) {
      return val.trim() !== '';
    },

    isName(val) {
      const nameRegex = /^[\wÀ-ÿ\u00f1\u00d1'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/;
      return nameRegex.test(val.trim());
    },

    isEmail(val) {
      const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return emailRegex.test(val);
    },

    isInteger(val) {
      const numericRegex = /^[0-9]+$/;
      return numericRegex.test(val);
    },

    isAlphaNumeric(val) {
      const alphaNumericRegex = /^[0-9a-zA-Z]+$/;
      return alphaNumericRegex.test(val);
    },
  };

  const rules = {
    names: validations.isName,
    email: validations.isEmail,
    numbers: validations.isInteger,
    required: validations.isNotEmpty,
    alphanumeric: validations.isAlphaNumeric,
  };

  const validate = {
    init(data, callback) {
      options = data;
      cb = callback;
      const form = document.querySelector(options.form);

      if (!form) {
        throw new Error('Can´t find form selector');
      }

      const action = form.querySelector(options.action);
      let fieldsArr;

      if (options.fields) {
        fieldsArr = [];

        options.fields.forEach((fieldObj) => {
          const fields = form.querySelectorAll(fieldObj.selector);

          Array.prototype.forEach.call(fields, (field) => {
            fieldsArr.push({
              el: field,
              rule: fieldObj.rule,
              errorMessage: fieldObj.message,
              selector: fieldObj.selector,
            });
          });
        });

        fieldsArr = fieldsArr.filter(obj => obj.el);

        // fieldsArr = options.fields.map(fieldObj => ({
        //   el: form.querySelector(fieldObj.selector),
        //   rule: fieldObj.rule,
        //   errorMessage: fieldObj.message,
        //   selector: fieldObj.selector,
        // })).filter(obj => obj.el);
      }

      validate._blurHandle(fieldsArr);

      if (action) {
        validate._clickHandle(fieldsArr, action, options); // for specific button
      } else {
        validate._submitHandle(form, fieldsArr, options); // for submit event
      }
    },

    isValid(selector) {
      const el = document.querySelectorAll(selector)[0];
      const fieldErrorClass = options.fieldErrorClass || 'field-error';


      return (el.querySelectorAll(`.${fieldErrorClass}`).length === 0);
    },

    /**
         * Validation when file is blur
         * @param  {[Array]} Array of Objects
         */
    _blurHandle(fieldsArr) {
      const self = this;

      fieldsArr.forEach((fieldObj) => {
        if (!fieldObj.el) {
          throw new Error(`Can't find ${fieldObj.selector} selector`);
        }

        fieldObj.el.addEventListener('blur', () => {
          self._errorHandle(fieldObj.el, fieldObj.rule, fieldObj.errorMessage);
        });
      });
    },

    /**
         * Validation by button click, validation without submit
         * @param  {[Array]} Array of Objects
         * @param  {[el]} button el
         */
    _clickHandle(fieldsArr, button) {
      const self = this;

      button.addEventListener('click', (evt) => {
        fieldsArr.forEach((fieldObj) => {
          self._errorHandle(fieldObj.el, fieldObj.rule, fieldObj.errorMessage);
        });
      });
    },

    /**
         * Validation when form is submited
         * @param  {string} form   is the selector of the form
         * @param  {array} fields is an array with the field selector, type of validation and error message
         */
    _submitHandle(form, fieldsArr) {
      const self = this;

      form.addEventListener('submit', (evt) => {
        evt.preventDefault();

        fieldsArr.forEach((fieldObj) => {
          self._errorHandle(fieldObj.el, fieldObj.rule, fieldObj.errorMessage);
        });
      });
    },

    _errorHandle(field, rule, message) {
      const currentClass = field.className;
      const errorMsg = document.createElement('small');
      errorMsg.className = `${options.fieldErrorClass}-message`;

      const { parentNode } = field;
      const fieldErrorClass = options.fieldErrorClass || 'field-error';

      if (rules[rule](field.value)) {
        field.className = currentClass.replace(fieldErrorClass, '');
        const currentErrorMsg = parentNode.querySelector('small');
        if (currentErrorMsg) {
          parentNode.removeChild(parentNode.querySelector('small'));
        }
      } else if (currentClass.indexOf(fieldErrorClass) === -1) {
        field.className = `${currentClass} ${fieldErrorClass}`;
        field.parentNode.appendChild(errorMsg);
        errorMsg.innerHTML = message;
      }

      if(cb) cb();
    },
  };

  return {
    init: validate.init,
    isValid: validate.isValid,
    rules,
  };
}));
