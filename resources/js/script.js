
import nav from './components/nav';
import form from './components/form';
import dropdown from './components/dropdown';
import modal from './components/modal';
import validations from './validations';
import notification from './components/notification';

document.addEventListener('DOMContentLoaded', () => {
  // DOM loaded
  nav();
  form();
  dropdown();
  modal();
  validations();
  notification();
});

window.addEventListener('load', () => {
  // All resources loaded
});
