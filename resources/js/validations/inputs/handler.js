import { isDouble, isInteger } from './validation';

const onlyInteger = (e) => {
  if (!isInteger()) e.preventDefault();
};

const onlyDouble = (e) => {
  if (!isDouble()) e.preventDefault();
};

export default {
  onlyInteger,
  onlyDouble,
};
