export const isInteger = (event) => {
  const newEvent = event || window.event;
  const key = newEvent.keyCode;
  return (key >= 48 && key <= 57); // Allow number
};

export const isDouble = (event) => {
  const newEvent = event || window.event;
  const key = newEvent.keyCode;
  return (key >= 48 && key <= 57) || key === 46; // Allow number
};
