//  sure assign input id
const inputType = (type, method) => {
  const inputs = document.querySelectorAll(`input[type=${type}]`);
  if (!inputs) return;

  Array.prototype.forEach.call(inputs, (input) => {
    input.addEventListener('keypress', method);
  });
};

export default {
  inputType,
};
