import validate from './inputs/index';
import handler from './inputs/handler';

const validationInputs = () => {
  validate.inputType('integer', handler.onlyInteger);
  validate.inputType('double', handler.onlyDouble);
};

const validations = () => {
  validationInputs();
};

export default validations;
