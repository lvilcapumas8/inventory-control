<div class="es-header">

  <div class="columns is-mobile is-gapless">
    <div class="column is-3-tablet is-2-mobile is-hidden-desktop">
      <div class="es-hamburger js-es-hamburger">
        <span class="es-hamburger__bar"></span>
        <span class="es-hamburger__bar"></span>
        <span class="es-hamburger__bar"></span>
      </div>
    </div>
    <div class="column is-8-mobile is-6-tablet is-2-desktop is-3-fullhd">
      <div class="es-header__logo">
        <a href="#"><img src="{{ asset('images/tektonlabs.png') }}" width="75" alt="logo"></a>
      </div>
    </div>

    <div class="column is-8-desktop is-6-fullhd columns is-gapless is-hidden-touch">
      <div class="column is-3">
        <a href="{{ route('home.index') }}"
          class="es-header__title {{ request()->is('inicio/*') || request()->is('inicio') ? 'active' : '' }}">
          <label>Inicio</label>
        </a>
      </div>
      <div class="column is-3">
        <a href="{{ route('register.index') }}" 
          class="es-header__title {{ request()->is('registro/*') || request()->is('registro') ? 'active' : '' }}">
          <label>Registro</label>
        </a>
      </div>
      <div class="column is-3">
        <a href="{{ route('inventory.index') }}"
          class="es-header__title {{ request()->is('inventario/*') || request()->is('inventario') ? 'active' : '' }}">
          <label>Inventario</label>
        </a>
      </div>
      <div class="column is-3">
        <a href="{{ route('report.index') }}"
          class="es-header__title {{ request()->is('reporte/*') || request()->is('reporte') ? 'active' : '' }}">
          <label>Reportes</label>
        </a>
      </div>
    </div>

    <div class="column is-2-mobile is-3-tablet is-2-desktop is-3-fullhd is-hidden-touch">
      <div class="es-avatar">
        <div class="es-avatar-notification">O</div>
        <div class="es-avatar__wrapper">
          <div class="es-avatar__photo">
            <img src="{{ asset('images/avatar.png') }}" alt="photo">
          </div>
          <div class="es-avatar__label">
            Miguelillo
            <span>V</span>
          </div>
        </div>
      </div>
    </div>
    {{-- <div class="column is-3-tablet is-2-mobile">
      <div class="es-header__buttons">
          <a href="#" class="es-btn es-btn--white">Suscríbase</a>
          <div class="es-dropdown es-dropdown--header js-es-dropdown">
            <span class="es-dropdown__target js-es-dropdown-target">Miguel</span>
            <ul class="es-dropdown__list">
              <li><a href="#"><i class="es-icon-account"></i> Mi cuenta</a></li>
              <li><a href="#"><i class="es-icon-card"></i> Mi membresía</a></li>
              <li><a href="#"><i class="es-icon-tag-o"></i> Mis artículos guardados</a></li>
              <li><a href="#" class="text-red"><i class="es-icon-logout"></i> Cerrar sesión</a></li>
            </ul>
          </div>
          <a href="#" class="es-btn-o es-btn-o--white">Iniciar sesión</a>
      </div>
    </div> --}}
  </div>

  <div class="es-reading-bar is-hidden-tablet">
    <div class="es-reading-bar__progress js-es-reading-bar__progress"></div>
  </div>
</div>
