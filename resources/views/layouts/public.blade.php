<!doctype html>
<html lang="{{ app()->getLocale() }}" class="{{ session('modalOpen') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description')">
    <meta property="og:title" content="@yield('title') | Electrónica Solano">
    <meta property="og:description" content="@yield('description')">
    @hasSection('image')
    <meta property="og:image" content="@yield('image')">
    @else
    <meta property="og:image" content="{{ asset('img/mocks/article-image-sm.jpg') }}">
    @endif
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:site_name" content="Electrónica Solano">
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@SEMANAeconomica" />
    <meta name="twitter:title" content="@yield('title') | Electrónica Solano" />
    <meta name="twitter:description" content="@yield('description')" />
    @hasSection('image')
    <meta property="twitter:image" content="@yield('image')">
    @else
    <meta property="twitter:image" content="{{ asset('img/mocks/article-image-sm.jpg') }}">
    @endif
    <link rel="icon" href="#"/>
    <title>@yield('title') | Electrónica Solano</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  </head>

  <body class="@yield('body_class') js-es-body">
    <div id="app">
      @include('layouts._header')
      @include('layouts._nav')
      
      <div class="container">
        @include('layouts._notification')
        @yield('content')
      </div>
    </div>
  </body>

  <script src="{{ mix('js/app.js') }}"></script>
</html>