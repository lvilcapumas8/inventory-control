<div class="es-nav js-es-nav">
  <div class="is-hidden-desktop">
    <ul class="es-nav__menu">
        <li class="es-dropdown es-dropdown--nav js-es-dropdown">
          <a class="es-dropdown__target js-es-dropdown-target bg-lighter-grey">Miguelito</a>
          <ul class="es-dropdown__list">
            <li><a href="#"><i class="es-icon-account"></i> Mi cuenta</a></li>
            <li><a href="#"><i class="es-icon-card"></i> Mi membresía</a></li>
            <li><a href="#"><i class="es-icon-tag-o"></i> Mis artículos guardados</a></li>
            <li><a href="#"><i class="es-icon-logout"></i> Cerrar sesión</a></li>
          </ul>
        </li>
        <li><a href="#">Suscríbase</a></li>
        <li><a href="#">Iniciar sesión</a></li>
    </ul>
  </div>
</div>