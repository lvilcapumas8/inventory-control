@if(session('error_message'))
  <div class="notification is-danger">
    <button class="delete"></button>
    {{ session('error_message') }}
  </div>
  @endif

@if(session('success_message'))
  <div class="notification is-success">
    <button class="delete"></button>
    {{ session('success_message') }}
  </div>
@endif