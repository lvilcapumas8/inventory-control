@extends('layouts.public')
@section('body_class', 'home')
@section('title', 'Portal de negocios y economía')
@section('home', 'active')

@section('content')
<div class="es-section">
  <div class="es-section__title mb20">
    Registro de Inventario
  </div>
  <div class="es-section__description">
    Visualización de último producto registrado
    <span class="is-pull-right"><a class="button is-info js-es-modal-toggle-button" data-modal=".js-es-modal-product-add">Agregar Marca</a></span>
    <span class="is-pull-right"><a class="button is-info js-es-modal-toggle-button" data-modal=".js-es-modal-product-add">Agregar Producto</a></span>
  </div>
</div>

<div class="es-section">
  <div class="es-card is-flex">
    <div class="es-card-last-article">
      <div>
        <label class="es-card-last-article__title mv5">Nombre</label>
        <label class="es-card-last-article__body mv5">Mi libro</label>
      </div>
      <div>
        <label class="es-card-last-article__title mv5">Código</label>
        <label class="es-card-last-article__body mv5">123000</label>
      </div>
      <div>
        <label class="es-card-last-article__title mv5">Código</label>
        <label class="es-card-last-article__body mv5">123000</label>
      </div>
    </div>

    <div class="es-card-last-article">
      <div>
        <label class="es-card-last-article__title mv5">Marca</label>
        <label class="es-card-last-article__body mv5">LG</label>
      </div>
      <div>
        <label class="es-card-last-article__title mv5">Imagen</label>
        <label class="es-card-last-article__body mv5y">buehaha</label>
      </div>
      <div>
        <label class="es-card-last-article__title mv5">Imagen</label>
        <label class="es-card-last-article__body mv5">buehaha</label>
      </div>
    </div>
  </div>
</div>

<div class="es-section">
  <div class="es-section__subtitle">
    Historial de Registros
  </div>
  <div class="es-section__description">
      <table-dropdown></table-dropdown>
  </div>
</div>
</div>

<div class="es-modal es-modal--sm es-modal--vcentered js-es-modal js-es-modal-product-add">
<div class="es-modal__content has-text-centered">
  <span class="es-modal__close js-es-modal-toggle-button" data-modal=".js-es-modal-product-add">
    <img src="{{ asset('images/cancel.svg') }}" alt="exit">
  </span>
  <h3 class="es-modal__title">Agregar Producto</h3>
  <p class="es-modal__description"></p>

  <form action="{{ route('register.product.store') }}" method="POST" class="js-es-register-product-form" enctype="multipart/form-data">
    @csrf

    <div class="field">
      <div class="control">
        <input class="input es-input-name" name="name" type="text" placeholder="Nombre">
      </div>
    </div>
    <div class="field">
      <div class="control">
          <div class="select is-fullwidth">
            <select>
              <option hidden>Seleccione marca</option>
              @foreach ($brands as $brand)
              <option value="{{ $brand->id }}">{{ $brand->name }}</option>
              @endforeach
            </select>
          </div>
      </div>
    </div>
    <div class="field">
      <div class="control">
        <input class="input es-input-price" type="double" name="price" placeholder="Precio Venta">
      </div>
    </div>
    <div class="field">
      <div class="control">
        <textarea class="textarea has-fixed-size es-input-description" rows="4" type="text" name="description" placeholder="Descripción"></textarea>
      </div>
    </div>
    <div class="file mb20">
      <label class="file-label d-block">
        <input class="file-input es-input-file" type="file" name="image" accept="image/jpg, image/jpeg, image/png" />
        <span class="file-cta">
          <span class="file-icon">
            <i class="fa fa-upload"></i>
          </span>
          <span class="file-label">
            Selecione Imagen…
          </span>
        </span>
      </label>
    </div>
    
    <div class="es-modal__actions">
      <button class="button js-es-modal-toggle-button" data-modal=".js-es-modal-product-add">Cancelar</button>
      <button type="submit" class="button is-info">Guardar</button>
    </div>
  </form>
</div>
@endsection
