@extends('layouts.public')
@section('body_class', 'home')
@section('title', 'Portal de negocios y economía')
@section('home', 'active')

@section('content')
  <div class="container">
    
    <div class="es-section">
      <div class="es-section__title">
        ¡Hola, Miguel!
      </div>
      <div class="es-section__description">
        <div class="es-input-icon">
          <input class="es-input__search" type="text" value="miguelito">
          <span class="es-input__icon-search" aria-hidden="true"></span>
        </div>

      </div>
    </div>

    <div class="es-section">
      <div>
        <example-component></example-component>
      </div>
    </div>
  </div>
@endsection
