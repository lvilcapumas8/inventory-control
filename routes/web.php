<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();

Route::group(['prefix' => 'inicio'], function () {
    Route::get('/', 'HomeController@index')->name('home.index');
});
Route::group(['prefix' => 'registro'], function () {
    Route::get('/', 'RegisterController@index')->name('register.index');
    Route::group(['prefix' => 'producto'], function () {
        Route::post('/guardar', 'RegisterController@storeProduct')->name('register.product.store');
    });
});
Route::group(['prefix' => 'inventario'], function () {
    Route::get('/', 'InventoryController@index')->name('inventory.index');
});
Route::group(['prefix' => 'reporte'], function () {
    Route::get('/', 'ReportController@index')->name('report.index');
});
