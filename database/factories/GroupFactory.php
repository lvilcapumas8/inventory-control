<?php

use App\Models\Group;
use Faker\Generator as Faker;
use App\Helpers\ModelFactoryHelper;
use App\Models\Category;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'category_id' => ModelFactoryHelper::getOrCreate(Category::class),
    ];
});
