<?php

use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    $document = $faker->randomElement(['dni', 'ce']);

    return [
        'name' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'document' => $document,
        'document_number' => $document == 'dni' ?
            $faker->numerify('########') : $faker->numerify('########'),
        'address' => $faker->address,
    ];
});
