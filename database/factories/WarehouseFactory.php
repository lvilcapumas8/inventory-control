<?php

use App\Models\Warehouse;
use Faker\Generator as Faker;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        'name' => 'Almacén '.$faker->word,
        'address' => $faker->address,
    ];
});
