<?php

use App\Models\Group;
use App\Models\Product;
use App\Models\ProductGroup;
use Faker\Generator as Faker;
use App\Helpers\ModelFactoryHelper;

$factory->define(ProductGroup::class, function (Faker $faker) {
    return [
        'product_id' => ModelFactoryHelper::getOrCreate(Product::class),
        'group_id' => ModelFactoryHelper::getOrCreate(Group::class),
    ];
});
