<?php

use App\Models\Product;
use App\Models\ProductWarehouse;
use App\Models\Warehouse;
use Faker\Generator as Faker;
use App\Helpers\ModelFactoryHelper;

$factory->define(ProductWarehouse::class, function (Faker $faker) {
    return [
        'warehouse_id' => ModelFactoryHelper::getOrCreate(Warehouse::class),
        'product_id' => ModelFactoryHelper::getOrCreate(Product::class),
        'stock' => $faker->numerify('##'),
    ];
});
