<?php

use App\Models\Brand;
use App\Models\Product;
use Faker\Generator as Faker;
use App\Helpers\ModelFactoryHelper;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->countryCode,
        'brand_id' => ModelFactoryHelper::getOrCreate(Brand::class),
        'image' => $faker->url,
    ];
});
