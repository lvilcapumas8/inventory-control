<?php

use App\Models\Product;
use App\Models\StockMovement;
use App\Models\User;
use App\Models\Warehouse;
use Faker\Generator as Faker;
use App\Helpers\ModelFactoryHelper;

$factory->define(StockMovement::class, function (Faker $faker) {
    return [
        'user_id' => ModelFactoryHelper::getOrCreate(User::class),
        'product_id' => ModelFactoryHelper::getOrCreate(Product::class),
        'warehouse_id' => ModelFactoryHelper::getOrCreate(Warehouse::class),
        'registered_at' => now(),
        'quantity' => 1,
        'in' => $faker->randomElement(['increase', 'decrease']),
    ];
});
