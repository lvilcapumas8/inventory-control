<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(BrandTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(WarehouseTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(GroupTableSeeder::class);
        $this->call(ProductGroupTableSeeder::class);
        $this->call(ProductWarehouseTableSeeder::class);
        $this->call(StockMovementTableSeeder::class);
    }
}
