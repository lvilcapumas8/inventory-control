<?php

use App\Models\ProductWarehouse;
use Illuminate\Database\Seeder;

class ProductWarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(ProductWarehouse::class, 12)->create();
    }
}
