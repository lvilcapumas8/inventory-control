<?php

use App\Models\StockMovement;
use Illuminate\Database\Seeder;

class StockMovementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(StockMovement::class, 3)->create();
    }
}
