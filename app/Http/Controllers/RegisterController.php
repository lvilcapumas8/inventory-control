<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        return view('screens.register.index', [
            'brands' => Brand::all(),
        ]);
    }

    public function storeProduct(Request $request)
    {
        try {
            \DB::beginTransaction();

            Product::create([
                'brand_id' => $request->brand_id,
                'name' => $request->name,
                'code' => $request->code,
                'description' => $request->description,
                'price' => $request->price,
                'image' => $this->storeImage($request),
            ]);

            \DB::commit();

            return back()->with(['success_message' => 'El producto se creó correctamente']);
        } catch (\Exception $e) {
            throw $e;

            return back()->with(['error_message' => 'Ocurrió un problema al crear producto']);
        }
    }

    /**
     * store image and return path to local.
     *
     * @param Request $request
     *
     * @return string
     */
    protected function storeImage(Request $request)
    {
        $fileName = Carbon::now()->timestamp;
        $extension = '.'.$request->image->clientExtension();
        $folder = 'products/';

        Storage::disk('local')->put($folder.$fileName.$extension, $request->image->get());

        return $folder.$fileName.$extension;
    }
}
