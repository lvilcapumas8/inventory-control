<?php

namespace App\Helpers;

class ModelFactoryHelper
{
    public static function getOrCreate($model, $qty = 1, $createNewRegistry = true)
    {
        if ($createNewRegistry) {
            if ($model::get()->isEmpty()) {
                if ($qty == 1) {
                    return factory($model)->create()->all()->random()->id;
                }

                return factory($model, $qty)->create()->random()->id;
            }

            return $model::get()->random()->id;
        }

        if ($qty == 1) {
            return factory($model)->create()->all()->random()->id;
        } elseif ($qty > 1) {
            return factory($model, $qty)->create()->random()->id;
        }
    }
}
